$(document).ready(function() {
    getWeatherData('40.440624,-79.995888');
    document
    .getElementById('search-btn')
    .addEventListener('click', function(event) {
        getWeatherData($('#weather-input').val());
    });
});

function getWeatherData(input) {
    $.ajax({
        url:
        'https://cors-anywhere.herokuapp.com/https://api.darksky.net/forecast/<API key>/' +
        input +
        '?exclude=minutely,hourly,alerts,flags'
    }).done(function(response) {
        setTimeDate(response.currently.time);
        $('#current-temp').text(Math.round(response.currently.temperature) + '°');
        $('#current-weather').text(response.currently.summary);
        $('#current-summary').text(response.daily.summary);
        setDayCard(response.daily.data);
    }).fail(function(data, status, error) {
        console.log('API Request Failure: ' + error);
    });
}

function setTimeDate(time) {
    var currentTimeDate = new Date(time * 1000);

    $('#current-date').text(
      currentTimeDate.toLocaleDateString('en-US', {
        weekday: 'long',
        year: 'numeric',
        month: 'long',
        day: 'numeric'
      })
    );

    $('#current-time').text(
      currentTimeDate.toLocaleTimeString('en-US', {
        hour: '2-digit',
        minute: '2-digit'
      })
    );
}

function setDayCard(data) {
    $('.forecast-row').html(
        data.slice(0, 5).map(function(day, index) {
            return (
                "<div class='col-12 col-lg mb-4'><div class='card'> \
                    <div class='card-body text-center'> \
                        <h5 class='card-title'>" + getDayTitle(day.time, index) + "</h5> \
                        <div class='row high-low-container'> \
                            <div class='col-12 icon-container mb-3 mt-2'> \
                                <i class='fas " + getIcon(day.icon) + " fa-3x'></i> \
                            </div> \
                            <div class='col-6 high'> \
                                <div>Hi:</div> \
                                <div>" + Math.round(day.temperatureHigh) + "&deg;</div> \
                            </div> \
                            <div class='col-6 low'> \
                                <div>Lo:</div> \
                                <div>" + Math.round(day.temperatureLow) + "&deg;</div> \
                            </div> \
                        </div> \
                    </div> \
                </div>"
            );
        })
    );
}

function getDayTitle(time, index) {
    if (index === 0) return 'Today';
    else return new Date(time * 1000).toLocaleDateString('en-US', {weekday: 'short'});
}

function getIcon(weather) {
    switch (weather) {
      case 'clear-day':
        return 'fas fa-sun';
      case 'clear-night':
        return 'fa-moon';
      case 'rain':
        return 'fa-cloud-showers-heavy';
      case 'snow':
        return 'fa-snowflake';
      case 'sleet':
        return 'cloud-showers-heavy';
      case 'wind':
        return 'fa-wind';
      case 'fog':
        return 'fa-smog';
      case 'cloudy':
        return 'fa-cloud';
      case 'partly-cloudy-day':
        return 'fa-cloud-sun';
      case 'partly-cloudy-night':
        return 'fa-cloud-moon';
      default:
        return 'fa-rainbow';
    }
}